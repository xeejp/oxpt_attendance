defmodule Oxpt.AttendanceTest do
  use Oxpt.CizenCase

  alias Oxpt.Attendance.Events.{
    ChangeShared
  }

  alias Oxpt.Attendance.Guest

  doctest Oxpt.Attendance.Guest
  doctest Oxpt.Attendance.Host

  setup do
    initial_state = %{
      players: %{
        "1" => %{
          finished: false,
          joined: true,
          read: false,
          status: "experiment",
          value: "A"
        },
        "2" => %{
          finished: false,
          joined: true,
          read: false,
          status: "experiment",
          value: "A"
        },
        "3" => %{
          finished: false,
          joined: true,
          read: false,
          status: "experiment",
          value: "A"
        }
      },
      shared: "A",
      page: "experiment"
    }

    {:ok, initial_state: initial_state}
  end


  test "Sharedを変化させたらsharedが変化する", %{initial_state: initial_state} do
    state = initial_state
    expected = state

    assert_handle(fn id ->
      expected1 = %{expected | shared: "B" }
      state1 = Guest.handle_event_body(id, %ChangeShared{game_id: id, value: "B"}, state)
      assert dropper(expected1) == dropper(state1)

      expected2 = %{expected1 | shared: "C" }
      state2 = Guest.handle_event_body(id, %ChangeShared{game_id: id, value: "C"}, state1)
      assert dropper(expected2) == dropper(state2)

      expected3 = %{expected2 | shared: "A" }
      state3 = Guest.handle_event_body(id, %ChangeShared{game_id: id, value: "A"}, state2)
      assert dropper(expected3) == dropper(state3)
    end)
  end


  def dropper(map) do
    Map.drop(map, [:players])
  end

end
