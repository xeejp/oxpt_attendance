defmodule Oxpt.Attendance.Locales do
  def get do
    %{
      en: %{
        translations: %{
          variables: %{
            shared_01: "Shared",
            own_01: "Own",
            test_01: "Test",
            round_01_s: "round",
            round_01_m: "rounds",
          },
          host: %{
            title_01: "Attendance",
            stepper: %{
              waiting_page_01: "Waiting",
              instruction_page_01: "Instruction",
              experiment_page_01: "Experiment",
              result_page_01: "Result",
              back_01: "BACK",
              next_01: "NEXT",
            },
            setting: %{
              title_01: "Setting",
              send_01: "SEND",
              cancel_01: "CANCEL",
              en_01: "English",
              ja_01: "Japanese",
            }
          },
          guest: %{
            instruction: %{
              instruction_title_01: "Instruction",
              instructions_01: [
                "<p>Instruction 1</p><p>Instruction 2</p>",
                "<p>Instruction 3</p>",
                "<p>Instruction 4</p>"
              ],
            },
            experiment: %{
              instruction_01_s: "{{num}}_{{round_01_s}}",
              instruction_01_m: "{{num}}_{{round_01_m}}",

              error: %{
                cant_join_01: "Could not join the game. Please wait for this game to end.",
              }
            },
          }
        }
      },
      ja: %{
        translations: %{
          variables: %{
            shared_01: "シェア",
            own_01: "オウン",
            test_01: "テスト",
            round_01_s: "ラウンド",
            round_01_m: "ラウンド",
          },
          host: %{
            title_01: "ボイラープレート",
            stepper: %{
              waiting_page_01: "待機",
              instruction_page_01: "説明",
              experiment_page_01: "実験",
              result_page_01: "結果",
              back_01: "戻る",
              next_01: "次へ",
            },
            setting: %{
              title_01: "設定",
              send_01: "送信",
              cancel_01: "キャンセル",
              en_01: "英語",
              ja_01: "日本語",
            },
          },
          guest: %{
            instruction: %{
              instruction_title_01: "ゲームの説明",
              instructions_01: [
                "<p>説明文その1</p><p>説明文その2</p>",
                "<p>説明文その3</p>",
                "<p>説明文その4</p>"
              ],
            },
            experiment: %{
              instruction_01_s: "{{num}}_{{round_01_s}}",
              instruction_01_m: "{{num}}_{{round_01_m}}",

              error: %{
                cant_join_01: "参加できませんでした。ゲームの終了をお待ち下さい。",
              }
            },
          }
        }
      }
    }
  end
end
