defmodule Oxpt.Attendance.Guest do
  @moduledoc """
  Documentation for Oxpt.Attendance.Guest
  """

  use Cizen.Automaton
  defstruct [:room_id]

  use Cizen.Effects
  use Cizen.Effectful
  alias Cizen.{Event, Filter}
  alias Oxpt.JoinGame
  alias Oxpt.Persistence
  alias Oxpt.Game
  alias Oxpt.Lounge.Host.AddedDummyGuest

  alias Oxpt.Attendance.{Host, Locales}

  alias Oxpt.Attendance.Events.{
    FetchState,
    UpdateStateSome,
    UpdateStateAll,
    Read,
    ChangePage,
    ChangeSetting,
    ChangeShared,
    ChangeOwn,
    ChangePage
  }

  use Oxpt.Game,
    root_dir: Path.join(__ENV__.file, "../../guest") |> Path.expand()

  @impl Oxpt.Game
  def player_socket(game_id, %__MODULE__{room_id: _room_id}, guest_id) do
    %__MODULE__.PlayerSocket{game_id: game_id, guest_id: guest_id}
  end

  @impl Oxpt.Game
  def new(room_id, _params) do
    %__MODULE__{room_id: room_id}
  end

  @impl Oxpt.Game
  def metadata(),
    do: %{
      label: "label_attendance",
      category: "category_other"
    }

  @impl true
  def spawn(id, %__MODULE__{room_id: room_id}) do
    Persistence.Game.setup(id, room_id)

    host_game_id =
      unless Application.get_env(:oxpt, :restoring, false) do
        perform(id, %Start{saga: Host.new(room_id, game_id: id)})
      else
        nil
      end

    perform(id, %Subscribe{
      event_filter:
        Filter.new(fn
          %Event{
            body: %FetchState{game_id: ^id}
          } ->
            true

          %Event{
            body: %AddedDummyGuest{game_id: ^id}
          } ->
            true

          %Event{
            body: %ChangePage{game_id: ^id}
          } ->
            true

          %Event{
            body: %Read{game_id: ^id}
          } ->
            true

          %Event{
            body: %ChangeSetting{game_id: ^id}
          } ->
            true

          %Event{
            body: %ChangeShared{game_id: ^id}
          } ->
            true

          %Event{
            body: %ChangeOwn{game_id: ^id}
          } ->
            true

          %Event{
            body: %JoinGame{game_id: ^id}
          } ->
            true
        end)
    })

    initial_state = %{
      room_id: room_id,
      host_game_id: host_game_id,
      host_guest_id: nil,
      guest_game_id: id,
      locales: Locales.get(),
      players: %{},
      shared: "A",
      page: "instruction",
      dummy_guest_list: [],
    }

    {:loop, initial_state}
  end

  @impl true
  def yield(id, {:loop, state}) do
    event = perform(id, %Receive{})

    new_state = handle_event_body(id, event.body, state)
    # IO.inspect Map.drop(new_state, [:locales])
    {:loop, new_state}
  end

  def handle_event_body(id, %JoinGame{guest_id: guest_id, host: true}, state) do
    perform(id, %Request{
      body: %JoinGame{game_id: state.host_game_id, guest_id: guest_id, host: true}
    })

    %{state | host_guest_id: guest_id}
  end

  def handle_event_body(id, %JoinGame{guest_id: guest_id}, state) do
    player = new_player(state)
    new_state = put_in(state, [:players, guest_id], player)

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          players: get_in(new_state, [:players])
        }
      }
    })

    new_state
  end

  def handle_event_body(id, %FetchState{guest_id: guest_id}, state) do
    perform(id, %Dispatch{
      body: %UpdateStateSome{
        game_id: id,
        guest_ids: [guest_id],
        state: state
      }
    })

    state
  end

  def handle_event_body(id, %AddedDummyGuest{dummy_guest_list: dummy_guest_list}, state) do
    new_state = put_in(state, [:dummy_guest_list], dummy_guest_list)

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          dummy_guest_list: get_in(new_state, [:dummy_guest_list])
        }
      }
    })

    new_state
  end

  def handle_event_body(id, %Read{guest_id: guest_id}, state) do
    new_state = put_in(state, [:players, guest_id, :read], true)

    perform(id, %Dispatch{
      body: %UpdateStateSome{
        game_id: id,
        guest_ids: [guest_id],
        state: %{
          players: get_in(new_state, [:players])
        }
      }
    })

    new_state
  end

  def handle_event_body(id, %ChangePage{page: page}, state) do
    new_state =
      state
      |> Map.put(:page, page)

    new_state =
      if state.page === "instruction" && page == "experiment" do
        # playersとsharedを変更
        reset(new_state)
      else
        new_state
      end
      |> set_status(page)

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          page: get_in(new_state, [:page]),
          players: get_in(new_state, [:players]),
          shared: get_in(new_state, [:shared])
        }
      }
    })

    new_state
  end

  def handle_event_body(id, %ChangeSetting{payload: payload}, state) do
    new_state =
      state |> update_locales(payload["locales_temp"])
            |> reset

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: new_state
      }
    })

    new_state
  end

  def handle_event_body(id, %ChangeShared{value: value}, state) do
    new_state = put_in(state, [:shared], value)

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          shared: get_in(new_state, [:shared])
        }
      }
    })

    new_state
  end

  def handle_event_body(id, %ChangeOwn{guest_id: guest_id, value: value}, state) do
    new_state = put_in(state, [:players, guest_id, :value], value)

    perform(id, %Dispatch{
      body: %UpdateStateSome{
        game_id: id,
        guest_ids: [guest_id],
        state: %{
          players: get_in(new_state, [:players])
        }
      }
    })

    new_state
  end

  def reset(state) do
    # playersとsharedを変更

    new_players = Enum.reduce(Map.keys(state.players), state.players, fn key, acc_players->
      acc_players |>
        update_in([key], fn player ->
          player
          |> Map.merge(new_player(state))
          |> Map.merge(%{joined: true})
        end)
    end)

    state
      |> Map.merge(%{
        players: new_players,
        shared: "A"
      })
  end

  def set_status(state, page) do
    new_players = Enum.reduce(Map.keys(state.players), state.players, fn key, acc_players->
      acc_players |>
        update_in([key], fn player ->
          player |> Map.merge(%{
            status: case page do
              "experiment" -> "experiment"
              _ -> nil
            end
          })
        end)
    end)

    state
      |> Map.merge(%{
        players: new_players
      })
  end

  def update_locales(state, locales_temp) do
    # localeを変更
    Enum.reduce(Map.keys(locales_temp), state, fn key, acc_state ->
      lang_temp = locales_temp[key]

      update_in(
        acc_state,
        [:locales, String.to_existing_atom(key), :translations],
        fn translations ->
          Enum.reduce(Map.keys(lang_temp), translations, fn trans_key, acc_trans ->
            put_in(acc_trans, [:variables, String.to_existing_atom(trans_key)], lang_temp[trans_key])
          end)
        end
      )
    end)
  end

  def new_player(state) do
    %{
      value: "A",
      status: nil,
      read: false,
      finished: false,
      joined: state.page != "experiment" && state.page != "result"
    }
  end
end
