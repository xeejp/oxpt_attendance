import React, { useReducer, useEffect } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import HCExporting from 'highcharts/modules/exporting'

import i18nInstance from '../i18n'

HCExporting(Highcharts)

export default () => {
  const { locales, players } = useStore()

  const [, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(locales && players))
    return <></>

  const ownA = Object.keys(players).filter(key => players[key].value === 'A').length
  const ownB = Object.keys(players).filter(key => players[key].value === 'B').length
  const ownC = Object.keys(players).filter(key => players[key].value === 'C').length

  return (
    <HighchartsReact
      options={{
        title: {
          text: ''
        },
        tooltip: {
          pointFormat: '<b>{point.y}</b>'
        },
        plotOptions: {
          pie: {
            dataLabels: {
              enabled: true,
              format: '<b>{point.name}</b>: {point.y}'
            }
          }
        },
        series: [
          {
            type: 'pie',
            innerSize: '30%',
            data: [
              {
                name: 'A',
                y: ownA,
                color: '#7cb5ec'
              },
              {
                name: 'B',
                y: ownB,
                color: '#f7a35c'
              },
              {
                name: 'C',
                y: ownC,
                color: '#90ed7d'
              }
            ]
          }
        ],
        credits: {
          enabled: false
        },
        chart: {
          height: '100%',
        },
        exporting: {
          buttons: {
            contextButton: {
              symbol: 'menu',
              menuItems: ['downloadPNG']
            }
          },
          fallbackToExportServer: false
        }
      }}
      highcharts={Highcharts}
    />
  )
}
