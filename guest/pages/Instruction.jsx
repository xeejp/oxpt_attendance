import React, { useState, useRef } from 'react'
import QrReader from 'react-qr-reader'
import Grid from '@material-ui/core/Grid'

import FlipCameraIosIcon from '@material-ui/icons/FlipCameraIos'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import IconButton from '@material-ui/core/IconButton'

import { makeStyles, useTheme } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
  button: {
    padding: theme.spacing(2)
  }
}))

export default () => {
  const classes = useStyles()
  const theme = useTheme()
  const [scanResult, setScanResult] = useState('No result')
  const [on, setOn] = useState(true)
  const [facingMode, setFacingMode] = useState('environment')
  const [legacyMode] = useState(false)
  const qrReaderRef = useRef()

  let maxSize = Math.min(document.body.clientWidth, document.body.clientHeight) - theme.spacing(2)

  const handleScan = (data) => {
    if (data) {
      let dataSplit = data.split(':')
      if (dataSplit[1] <= Date.now()) {
        setScanResult(data)
        // ここに学生証番号入力画面へ遷移するためのコードを書く
      } else {
        console.log('時間切れやで')
      }
    }
  }

  const handleError = (err) => {
    console.error(err)
  }

  const handleOn = () => {
    setOn(!on)
  }

  const handleChangeCamera = () => {
    if (facingMode === 'environment') {
      setFacingMode('user')
    } else {
      setFacingMode('environment')
    }
  }

  return (
    <>
      <Grid item>
        <p>{scanResult}</p>
        <Button variant="contained" color="primary" onClick={handleOn}>{on ? 'Camera Off' : 'Camera On'}</Button>
        {(scanResult === 'No result')
          ? <>
            <Button variant="outlined" className={classes.button} onClick={() => qrReaderRef.current.openImageDialog.bind(this)}>画像ファイル選択</Button>
            <IconButton color="primary" aria-label="Change camera mode" onClick={handleChangeCamera}>
              <FlipCameraIosIcon />
            </IconButton>
            <QrReader
              ref={qrReaderRef}
              delay={300}
              onError={handleError}
              onScan={handleScan}
              legacyMode={legacyMode}
              maxImageSize={maxSize}
              facingMode={facingMode}
            />
          </>
          : <>
            <TextField id="outlined-basic" label="学生証番号" variant="outlined" />
            <Button variant="contained" className={classes.button}>送信</Button>
          </>
        }
      </Grid>
    </>
  )
}
